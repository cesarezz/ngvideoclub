require('angular');
require("angular-route");
require('angular-i18n/angular-locale_es');

require('./modules/principal/principal.module');
require('./modules/header/header.module');
require('./modules/prestamo/prestamo.module');
require('./modules/cliente/cliente.module');
require('./modules/copia/copia.module');
require('./modules/principalCliente/principalCliente.module');
require('./modules/principalAdministrador/principalAdministrador.module');
require('./modules/pelicula/pelicula.module');
require('./modules/util/util.module');


angular.module('videoclub',['ngRoute','principal.module','header.module','pelicula.module','principalCliente.module','copia.module','cliente.module','prestamo.module','util.module','principalAdministrador.module'])
.controller('globalController',['headers','headerService',globalController])
.value("accessPoint","http://localhost:8080/Videoclub")
.config(['$routeProvider', rutas]);

function globalController(headers,headerService){
	
	this.headers = headers;
	this.headerService = headerService;
}

function rutas($routeProvider){

  $routeProvider.otherwise({
    redirectTo: '/principal'
  });

}

