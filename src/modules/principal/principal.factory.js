
function PrincipalFactory(){
	
	var Principal = function(){
		this.mensaje = "principal";

		this.usuario = null;

		this.contraseña = null;

		this.errorAutenticacion = false;
	};

	return new Principal();

}
