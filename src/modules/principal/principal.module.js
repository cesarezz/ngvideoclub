/** Modulo header */
require('./principal.controller');
require('./principal.factory');
require('./principal.service.js');
require('./principal.config.js');

angular.module('principal.module',[])

	.factory('principal',PrincipalFactory)

	.service('principalService',[
		'principal',
		'clienteService',
		'$location',
		'principalCliente',
		'headers',
	PrincipalService])

	.config(['$routeProvider',configuracionRutasPrincipal])

	.controller('principalController',[
		'principal',
		'principalService',
		'pelicula',
	principalController]);
