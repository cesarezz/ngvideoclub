/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function principalController(principal,principalService,pelicula){

	var self = this;
	self.principal = principal;
	self.principalService = principalService;
	self.pelicula = pelicula;

}