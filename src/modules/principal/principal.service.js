/**
 * Created by csanjose on 30/08/2016.
 */
function PrincipalService(principal, clienteService, $location, principalCliente, headers){
    this.principal = principal;
    this.clienteService = clienteService;
    this.$location = $location;
    this.principalCliente = principalCliente;
    this.headers = headers;
}

angular.extend(PrincipalService.prototype,{

    hello: function(){

        alert("hello Principal");
    },

    autenticacion: function(){

        var self = this;

        if(self.principal.usuario === "admin" && self.principal.password === "admin"){

            self.headers.rol = "administrador";
            self.$location.path( "/principalAdministrador");
            self.principal.errorAutenticacion = false;

        } else {

            self.clienteService.buscarPorDni(self.principal.usuario).then(function(result){

                self.principal.errorAutenticacion = false;

                self.validacionAutenticacion(result);

            }, function(error){

               alert(error);

            });

        }

    },

    validacionAutenticacion: function(cliente){

        var self = this;

        if(self.principal.password === cliente.password){

            self.principalCliente.cliente = cliente.dni;

            self.headers.rol = "cliente";

            self.$location.path( "/principalCliente" );

        } else {

            self.principal.errorAutenticacion = true;

        }

    }

});