/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasPrincipal($routeProvider){
    $routeProvider.when('/principal', {
        templateUrl: './views/principal.views.html',
        resolve: {

            cargaInicial: ['peliculaService','utilService', function(peliculaService,utilService) {
                peliculaService.cargar();
                utilService.scrollTop();
            }]

        }
    })
}