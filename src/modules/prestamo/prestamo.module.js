/** Modulo header */
require('./prestamo.controller');
require('./prestamo.factory');
require('./prestamo.service');
require('./prestamo.config');

angular.module('prestamo.module',[])

	.factory('prestamo',PrestamoFactory)

	.service('prestamoService',[
		'prestamo',
		'$filter',
		'$http',
		'accessPoint',
		'$q',
        'copiaService',
	PrestamoService])

	.config(['$routeProvider', configuracionRutasPrestamo])

	.controller('prestamoController',[
		'prestamo',
		'prestamoService',
		'utilService',
		'util',
	prestamoController]);
