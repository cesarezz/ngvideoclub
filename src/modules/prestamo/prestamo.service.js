
function PrestamoService(prestamo,$filter,$http,accessPoint, $q, copiaService){
	this.prestamo = prestamo;
	this.$filter = $filter;
	this.$http = $http;
	this.accessPoint = accessPoint;
	this.$q = $q;
    this.copiaService = copiaService;
}

angular.extend(PrestamoService.prototype,{

	cargar: function (){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/prestamos/').then(function(result){

			self.prestamo.data = self.setearFechas(result.data);
			deferred.resolve();

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;
	},

	setearFechas: function(prestamos){

		angular.forEach(prestamos, function(prestamo) {
			prestamo.recogida = new Date(prestamo.recogida);
			prestamo.devolucion = new Date(prestamo.devolucion);
		});

		return prestamos;
	},

	alquilar: function(cliente_id,copia_id, fecha_recogida, fecha_devolucion){

		var self = this;

		var post = {};
		post.cliente_id = cliente_id;
		post.copia_id = copia_id;
		post.recogida = fecha_recogida.getFullYear() + "-" + (fecha_recogida.getMonth()+1) + "-" + fecha_recogida.getDate();
		post.devolucion = new Date(fecha_devolucion).getFullYear() + "-" + ( new Date(fecha_devolucion).getMonth()+1) + "-" +  new Date(fecha_devolucion).getDate() + 7;
		post.estado = "alquilada";

		self.$http.post(self.accessPoint+'/prestamos/add',post).then(function(result){

            self.copiaService.alquilada(copia_id);

			self.cargar();

		}, function(error){
			alert(error);
		});

	},

	devolver: function(prestamo){

		var self = this;

		var deferred = self.$q.defer();

		var copia_id = prestamo.copia_id;

		var prestamoPost = angular.copy(prestamo);

		prestamoPost.estado = "devuelta";

		self.$http.put(self.accessPoint+'/prestamos/update',prestamoPost).then(function(result){

			self.cargar();

			if(copia_id != null){

				self.copiaService.devuelta(copia_id);

			}

			deferred.resolve();

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;
	},

	prestamoByClienteId: function (cliente_id){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/prestamos/cliente/' + cliente_id).then(function(result){

			var prestamos = self.setearFechas(result.data);
			deferred.resolve(prestamos);

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;
	},

});