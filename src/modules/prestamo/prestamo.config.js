/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasPrestamo($routeProvider){
    $routeProvider.when('/prestamos', {
        templateUrl: './views/prestamo.views.html',
        resolve: {

            cargaInicial: ['prestamoService','peliculaService','clienteService','copiaService','utilService','$q', function(prestamoService, peliculaService, clienteService, copiaService, utilService, $q) {

                var deferred = $q.defer();

                $q.all([
                    peliculaService.cargar(),
                    prestamoService.cargar(),
                    clienteService.cargar(),
                    copiaService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){

                    deferred.reject(error);

                });

                return deferred.promise;

            }]
        }
    });
}