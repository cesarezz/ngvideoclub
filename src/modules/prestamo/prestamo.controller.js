/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function prestamoController(prestamo,prestamoService, utilService, util){

	var self = this;

	self.prestamo = prestamo;
	self.prestamoService = prestamoService;
	self.utilService = utilService;
	self.util = util;

}