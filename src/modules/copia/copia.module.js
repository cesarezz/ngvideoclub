/** Modulo header */
require('./copia.controller');
require('./copia.factory');
require('./copia.service');
require('./copia.config');

angular.module('copia.module',[])

	.factory('copia',CopiaFactory)

	.service('copiaService',[
		'copia',
		'$filter',
		'$http',
		'accessPoint',
		'$q',
	CopiaService])

	.config(['$routeProvider',configuracionRutasCopia])

	.controller('copiaController',[
		'copia',
		'copiaService',
		'utilService',
	copiaController]);
