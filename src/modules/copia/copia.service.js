
function CopiaService(copia,$filter,$http,accessPoint,$q){
	this.copia = copia;
	this.$filter = $filter;
	this.$http = $http;
	this.accessPoint = accessPoint;
	this.$q = $q;
}

angular.extend(CopiaService.prototype,{

	cargar: function (){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/copias/').then(function(result){

			self.copia.data = result.data;
			deferred.resolve();

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;
	},

	alquilada: function (id_copia){

		var self = this;

		self.$http.get(self.accessPoint+'/copias/alquilada/'+ id_copia ).then(function(result){

			self.cargar();

		}, function(error){
			alert(error);
		});

	},

	devuelta: function (id_copia){

		var self = this;

		self.$http.get(self.accessPoint+'/copias/devuelta/'+ id_copia ).then(function(result){

			self.cargar();

		}, function(error){
			alert(error);
		});

	},

	comprar_copias: function(pelicula_id, num_copias){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/copias/pelicula/' + pelicula_id + '/cantidad/' + num_copias).then(function(result){

			self.cargar();
			deferred.resolve();

		}, function(error){

			alert(error);
			deferred.reject(error);

		});

		return deferred.promise;
	},

	eliminar_copias: function(id_pelicula){

		var self = this;

		var deferred = self.$q.defer();

		var copias_pelicula= angular.copy(self.$filter('filter')(self.copia.data , {pelicula_id: id_pelicula}, function(actual, expected) {
			if(actual === expected){
				return true;
			}
		}));

		var post = copias_pelicula;

		self.$http.post(self.accessPoint+'/copias/remove/array_copias', post).then(function(result){

			self.cargar();
			deferred.resolve();

		}, function(error){

			alert(error);
			deferred.reject(error);

		});

		return deferred.promise;

	},

	eliminar_copia: function(id_copia){

		var self = this;

		self.$http.delete(self.accessPoint+'/copias/remove/'+ id_copia).then(function(result){

			self.cargar();


		}, function(error){

			alert(error);


		});

	}

});