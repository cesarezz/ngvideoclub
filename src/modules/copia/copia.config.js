/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasCopia($routeProvider){
    $routeProvider.when('/copias', {
        templateUrl: './views/copia.views.html',
        resolve: {

            cargaInicial: ['copiaService','utilService', function(copiaService,utilService) {

                copiaService.cargar();

                utilService.scrollTop();

            }]
        }
    });
}