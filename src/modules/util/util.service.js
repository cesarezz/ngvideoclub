/**
 * Created by Cesarezz on 10/09/2016.
 */
function UtilService($filter, copia, prestamo, pelicula, cliente,$timeout,$location){
    this.$filter = $filter;
    this.copia = copia;
    this.prestamo = prestamo;
    this.pelicula = pelicula;
    this.cliente = cliente;
    this.$timeout = $timeout;
    this.$location = $location;
}

angular.extend(UtilService.prototype,{

    scrollTop: function(){

        window.scrollTo(0, 0);
    },

    fechaMinima: function(id_pelicula){

        var self = this;

        var copias_pelicula = angular.copy(self.$filter('filter')(self.copia.data , {pelicula_id: id_pelicula}, function(actual, expected) {
            if(actual === expected){
                return true;
            }
        }));

        var fecha_devolucion_min = null;

        angular.forEach(copias_pelicula, function(copia) {
            var id = copia.id;
            angular.forEach(self.prestamo.data, function (prestamo) {
                if(prestamo.copia_id === id && fecha_devolucion_min === null){
                    fecha_devolucion_min = prestamo.devolucion.getTime();
                } else if(prestamo.copia_id === id) {
                    fecha_devolucion_min = ((prestamo.devolucion.getTime() < fecha_devolucion_min) ? prestamo.devolucion : fecha_devolucion_min);
                }
            });

        });

        return ((fecha_devolucion_min === null) ? null : new Date(fecha_devolucion_min).toLocaleDateString());

    },

    buscarPelicula: function(id_pelicula){

        var self = this;

        var peliculas = null;

        if(id_pelicula !== null) {

            peliculas = angular.copy(self.$filter('filter')(self.pelicula.data, {id: id_pelicula}, function (actual, expected) {
                if (actual === expected) {
                    return true;
                }
            }));

        }

        return peliculas === null ? peliculas: peliculas[0];

    },

    peliculaByIdPrestamo: function(id_copia){

        var self = this;

        var copia = self.copiaById(id_copia);

        return copia === null ? copia : self.buscarPelicula(copia.pelicula_id);
    },

    copiaById: function(id_copia){

        var self = this;

        var copiasById = null;

        if(id_copia !== null){

            copiasById= angular.copy(self.$filter('filter')(self.copia.data , {id: id_copia}, function(actual, expected) {
                if(actual === expected){
                    return true;
                }
            }));

        }
        return copiasById === null ? copiasById: copiasById[0];
    },

    buscarClienteByDNI:function(dni_id){

        var self = this;

        var clienteByDNI= angular.copy(self.$filter('filter')(self.cliente.data , {dni: dni_id}, function(actual, expected) {
            if(actual === expected){
                return true;
            }
        }));

        return clienteByDNI[0];
    },

    delayRedireccion: function(ruta){

        var self = this;

        self.$timeout(function() {

            self.$location.path(ruta);

        }, 4000);


    }

});