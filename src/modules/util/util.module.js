/** Modulo header */
require('./util.controller');
require('./util.factory');
require('./util.service');

angular.module('util.module',[])

	.factory('util',UtilFactory)

	.service('utilService',[
		'$filter',
		'copia',
		'prestamo',
		'pelicula',
		'cliente',
		'$timeout',
		'$location',
	UtilService])

	.controller('utilController',[
		'util',
		'utilService',
	utilController]);
