/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function principalClienteController(principalCliente,principalClienteService,pelicula,prestamo,copia, util, utilService){

	var self = this;

	self.principalCliente = principalCliente;
	self.principalClienteService = principalClienteService;
	self.pelicula = pelicula;
	self.prestamo = prestamo;
	self.copia = copia;
	self.util = util;
	self.utilService = utilService;

}