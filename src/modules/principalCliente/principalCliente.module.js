/** Modulo header */
require('./principalCliente.controller.js');
require('./principalCliente.factory.js');
require('./principalCliente.service.js');
require('./principalCliente.config.js');

angular.module('principalCliente.module',[])

	.factory('principalCliente',PrincipalClienteFactory)
	.service('principalClienteService',[
		'$q',
		'$filter',
		'principalCliente',
		'pelicula',
		'copia',
		'prestamoService',
		'utilService',
		'$timeout',
		'peliculaService',
		PrincipalClienteService])

	.config(['$routeProvider', configuracionRutasPrincipalCliente])

	.controller('principalClienteController',[
		'principalCliente',
		'principalClienteService',
		'pelicula',
		'prestamo',
		'copia',
		'util',
		'utilService',
	principalClienteController]);
