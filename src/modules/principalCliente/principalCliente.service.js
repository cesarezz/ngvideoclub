
function PrincipalClienteService($q, $filter, principalCliente, pelicula, copia, prestamoService, utilService, $timeout, peliculaService){
	this.$q = $q;
	this.$filter = $filter;
	this.principalCliente = principalCliente;
	this.pelicula = pelicula;
	this.copia = copia;
	this.prestamoService = prestamoService;
	this.utilService = utilService;
	this.$timeout = $timeout;
	this.peliculaService = peliculaService;
}

angular.extend(PrincipalClienteService.prototype,{

	hello: function(){

		alert("hello Principal cliente");
	},

	addPeliculaCompra: function(pelicula){

		var self = this;

		var pelicula_final = angular.copy(pelicula);

		var peliculas_micompra= angular.copy(self.$filter('filter')(self.principalCliente.micompra , {pelicula_id: pelicula_final.id}, function(actual, expected) {
			if(actual === expected){
				return true;
			}
		}));

		if (peliculas_micompra.length === 0){

			delete pelicula_final.copia;
			delete pelicula_final.url;
			self.principalCliente.micompra.push(pelicula_final);

		}

	},

	confirmarCompra: function(){

		var self = this;

		angular.forEach(self.principalCliente.micompra, function (pelicula) {

			var copias_pelicula = angular.copy(self.$filter('filter')(self.copia.data , {pelicula_id: pelicula.id}, function(actual, expected) {
				if(actual === expected){
					return true;
				}
			}));

			var copias_pelicula_alquilables = angular.copy(self.$filter('filter')(copias_pelicula, {alquilada: false}, true));

			self.prestamoService.alquilar(self.principalCliente.cliente,copias_pelicula_alquilables[0].id, new Date(), new Date + 7 );

		});

		self.$timeout(function() {

			self.peliculaService.cargar();
			self.principalCliente.compraRealizada = true;

		}, 3000);

		self.$timeout(function() {

			self.principalCliente.micompra = [];
			self.principalCliente.compraRealizada = false;

		}, 6000);

	},

	peliculasCliente: function(){

		var self = this;

		var copia, pelicula, id_copia, peliculas_cliente = [];

        angular.forEach(self.principalCliente.prestamos, function(prestamo) {

			id_copia = prestamo.copia_id;

			//Busca la copia del prestamo
			copia = self.utilService.copiaById(id_copia);

			//Busca la película del prestamo
			pelicula = angular.copy(self.utilService.buscarPelicula(copia.pelicula_id));

			delete pelicula.copia;

			//Comprueba que esta peli no haya sido vista con anterioridad por el cliente
			var pelicula_vista= angular.copy(self.$filter('filter')(peliculas_cliente , {id: pelicula.id}, function(actual, expected) {
				if(actual === expected){
					return true;
				}
			}));

			if (pelicula_vista.length === 0){

				peliculas_cliente.push(pelicula);

			}

		});

		return peliculas_cliente;
	},

	devolver: function(prestamo){

		var self = this;

		self.prestamoService.devolver(prestamo).then(function(){

			self.prestamosByClienteId();

		}, function(error){

		});

	},

	prestamosByClienteId: function(){

		var self = this;

		self.prestamoService.prestamoByClienteId(self.principalCliente.cliente).then(function(results){

			self.principalCliente.prestamos = results;

		}, function(error){
			alert("Error");
		});

	}


});