/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasPrincipalCliente($routeProvider){
    $routeProvider.when('/principalCliente', {
        templateUrl: './views/principalCliente.views.html',
        resolve: {
            cargaInicial: ['principalClienteService','peliculaService','prestamoService','copiaService','utilService','$q', function(principalClienteService,peliculaService,prestamoService,copiaService,utilService,$q) {

                var deferred = $q.defer();

                $q.all([
                   peliculaService.cargar(),
                   prestamoService.cargar(),
                   principalClienteService.prestamosByClienteId(),
                   copiaService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }]

        }
    }).when('/principalCliente/peliculasCliente', {
        templateUrl: './views/peliculaCliente.views.html',
        resolve: {
            cargaInicial: ['principalClienteService','peliculaService','prestamoService','copiaService','utilService','$q', function(principalClienteService, peliculaService,prestamoService,copiaService, utilService, $q) {

                var deferred = $q.defer();

                $q.all([
                    peliculaService.cargar(),
                    //prestamoService.cargar(),
                    principalClienteService.prestamosByClienteId(),
                    copiaService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }]

        }
    }).when('/principalCliente/prestamosCliente', {
        templateUrl: './views/prestamoCliente.views.html',
        resolve: {
            cargaInicial: ['principalClienteService','peliculaService','prestamoService','copiaService','utilService','$q', function(principalClienteService,peliculaService,prestamoService,copiaService,utilService,$q) {

                var deferred = $q.defer();

                $q.all([
                    peliculaService.cargar(),
                    //prestamoService.cargar(),
                    principalClienteService.prestamosByClienteId(),
                    copiaService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }]

        }
    });
}