/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasCliente($routeProvider){

    $routeProvider.when('/clientes', {

        templateUrl: './views/cliente.views.html',

        resolve: {

            cargaInicial: ['clienteService','utilService', function(clienteService,utilService) {

                clienteService.cargar();
                utilService.scrollTop();

            }]
        }
    }).when('/registro', {

        templateUrl: './views/registro.views.html',

        resolve: {

            cargaInicial: ['utilService','clienteService', function(utilService, clienteService) {

                clienteService.resetearRegistro();

                utilService.scrollTop();

            }]
        }
    });
}