/** Modulo header */
require('./cliente.controller');
require('./cliente.factory');
require('./cliente.service');
require('./cliente.config');

angular.module('cliente.module',[])

	.factory('cliente',ClienteFactory)

	.service('clienteService',[
		'cliente',
		'$filter',
		'$http',
		'accessPoint',
		'$q',
		'$timeout',
		'$location',
		'utilService',
	ClienteService])

	.config(['$routeProvider',configuracionRutasCliente])

	.controller('clienteController',[
		'cliente',
		'clienteService',
	clienteController]);
