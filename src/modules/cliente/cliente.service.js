
function ClienteService(cliente,$filter,$http,accessPoint, $q, $timeout, $location,utilService){
	this.cliente = cliente;
	this.$filter = $filter;
	this.$http = $http;
	this.accessPoint = accessPoint;
	this.$q = $q;
	this.$timeout = $timeout;
	this.$location = $location;
	this.utilService = utilService;
}

angular.extend(ClienteService.prototype,{

	resetearRegistro: function(){

		var self = this;

		self.cliente.nuevo = {};

		self.cliente.mensaje = null;

	},

	cargar: function (){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/clientes/').then(function(result){

			self.cliente.data = result.data;
			deferred.resolve();

		}, function(error){
			alert(error);
			deferred.reject(error);

		});

	},

	buscarPorDni: function(dni){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/clientes/' + dni).then(function(result){

			deferred.resolve(result.data);

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;

	},

	registrar: function(formulario){

		var self = this;

		if(formulario.$valid){

			var data = angular.copy(self.cliente.nuevo);

			self.$http.post(self.accessPoint+'/clientes/add',data).then(function(result){

				self.cargar();

				self.cliente.mensaje = "Se ha autenticado correctamente, se le redireccionará a la página principal donde deberá autenticarse";

				self.utilService.delayRedireccion("/principal");

			}, function(error){

				alert(error);

			});

		}

	}

});