
function PrincipalAdministradorService( $filter, cliente){

	this.$filter = $filter;
	this.cliente = cliente;

}

angular.extend(PrincipalAdministradorService.prototype,{

	clienteByDni: function(cliente_id){

		var self = this;

		var clientesByDni= angular.copy(self.$filter('filter')(self.cliente.data , {dni: cliente_id}, function(actual, expected) {
			if(actual === expected){
				return true;
			}
		}));

		return clientesByDni[0];
	},

	diasDesdeDevolucion: function(fecha_devolucion){

		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var firstDate = new Date(fecha_devolucion);
		var secondDate = new Date();

		var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

		return diffDays;

	}

});