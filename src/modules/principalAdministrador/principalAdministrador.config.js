/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasPrincipalAdministrador($routeProvider){
    $routeProvider.when('/principalAdministrador', {
        templateUrl: './views/principalAdministrador.views.html',
        resolve: {

            cargaInicial: ['peliculaService','prestamoService','copiaService','clienteService','utilService','$q', function(peliculaService,prestamoService,copiaService,clienteService,utilService,$q) {

                var deferred = $q.defer();

                $q.all([
                   peliculaService.cargar(),
                   prestamoService.cargar(),
                   copiaService.cargar(),
                   clienteService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }]

        }

    });
}