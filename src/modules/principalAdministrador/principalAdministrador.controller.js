/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function principalAdministradorController(principalAdministrador,principalAdministradorService,pelicula,prestamo,copia, util, utilService){

	var self = this;

	self.principalAdministrador = principalAdministrador;
	self.principalAdministradorService = principalAdministradorService;
	self.pelicula = pelicula;
	self.prestamo = prestamo;
	self.copia = copia;
	self.util = util;
	self.utilService = utilService;

}