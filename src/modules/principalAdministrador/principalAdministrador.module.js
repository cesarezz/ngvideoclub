/** Modulo header */
require('./principalAdministrador.controller.js');
require('./principalAdministrador.factory.js');
require('./principalAdministrador.service.js');
require('./principalAdministrador.config.js');

angular.module('principalAdministrador.module',[])

	.factory('principalAdministrador',PrincipalAdministradorFactory)

	.service('principalAdministradorService',[
		'$filter',
		'cliente',
		PrincipalAdministradorService])

	.config(['$routeProvider', configuracionRutasPrincipalAdministrador])

	.controller('principalAdministradorController',[
		'principalAdministrador',
		'principalAdministradorService',
		'pelicula',
		'prestamo',
		'copia',
		'util',
		'utilService',
	principalAdministradorController]);
