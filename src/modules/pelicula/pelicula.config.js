/**
 * Created by Cesarezz on 02/09/2016.
 */
function configuracionRutasPelicula($routeProvider){
    $routeProvider.when('/peliculas', {
        templateUrl: './views/pelicula.views.html',
        resolve: {

            cargaInicial: ['peliculaService','copiaService','utilService','$q', function(peliculaService, copiaService, utilService, $q) {

                var deferred = $q.defer();

                $q.all([
                    peliculaService.cargar(),
                    copiaService.cargar()
                ]).then(function(results){

                    utilService.scrollTop();
                    deferred.resolve();

                }, function(error){

                    deferred.reject(error);

                });

                return deferred.promise;

            }]
        }
    }).when('/peliculas/add', {
        templateUrl: './views/create-pelicula.views.html',
        resolve: {
            cargaInicial: ['utilService','peliculaService', function(utilService, peliculaService) {

                peliculaService.resetearCreacionPelicula();

                utilService.scrollTop();

            }]
        }
    }).when('/peliculas/edit', {
        templateUrl: './views/edit-pelicula.views.html',
        resolve: {
            cargaInicial: ['utilService', function(utilService) {

                utilService.scrollTop();

            }]
        }
    });
}