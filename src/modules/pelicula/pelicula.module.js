/** Modulo header */
require('./pelicula.controller');
require('./pelicula.factory');
require('./pelicula.service');
require('./pelicula.config');

angular.module('pelicula.module',[])

	.factory('pelicula',PeliculaFactory)

	.service('peliculaService',[
		'pelicula',
		'$filter',
		'$http',
		'accessPoint',
		'$q',
		'copiaService',
		'utilService',
		'$location',
		'$timeout',
		'prestamoService',
	PeliculaService])

	.config(['$routeProvider',configuracionRutasPelicula])

	.controller('peliculaController',[
		'pelicula',
		'peliculaService',
	peliculaController]);
