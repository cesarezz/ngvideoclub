
function PeliculaService(pelicula,$filter,$http,accessPoint,$q, copiaService, utilService, $location, $timeout, prestamoService){
	this.pelicula = pelicula;
	this.$filter = $filter;
	this.$http = $http;
	this.accessPoint = accessPoint;
	this.$q = $q;
	this.copiaService = copiaService;
	this.utilService = utilService;
	this.$location = $location;
	this.$timeout = $timeout;
	this.prestamoService = prestamoService;
}

angular.extend(PeliculaService.prototype,{

	resetearCreacionPelicula: function(){

		var self = this;

		self.pelicula.nueva = {};

		self.pelicula.mensaje = null;

	},

	resetearEdicionPelicula: function(){

		var self = this;

		self.pelicula.editar = {};

		self.pelicula.mensaje = null;

	},

	cargar: function (){

		var self = this;

		var deferred = self.$q.defer();

		self.$http.get(self.accessPoint+'/peliculas/').then(function(result){

			self.pelicula.data = result.data;
			deferred.resolve();

		}, function(error){
			alert(error);
			deferred.reject(error);
		});

		return deferred.promise;

	},

	restarCopiaComprar: function(indice){

		var self = this;

		if(self.pelicula.array_compras[indice] >= 1){

			self.pelicula.array_compras[indice] = self.pelicula.array_compras[indice] - 1;

		}

	},

	sumarCopiaComprar: function(indice){

		var self = this;

		if(self.pelicula.array_compras[indice] <= 8){

			self.pelicula.array_compras[indice] = self.pelicula.array_compras[indice] + 1;

		}

	},

	comprar: function(indice, pelicula_id){

		var self = this;

		var num_copias = self.pelicula.array_compras[indice];

		var promise = self.copiaService.comprar_copias(pelicula_id, num_copias);

		promise.then(function() {

			self.cargar();

		}, function() {

			alert('Failed comprar copias');

		});

	},

	eliminar: function(pelicula_id){

		var self = this;

		var promise = self.copiaService.eliminar_copias(pelicula_id);

		promise.then(function() {

			self.eliminar_pelicula(pelicula_id);

		}, function() {

			alert('Failed remove copias');

		});



	},

	eliminar_pelicula: function(pelicula_id){

		var self = this;

		self.$http.delete(self.accessPoint+'/peliculas/remove/' + pelicula_id).then(function(result){

			self.cargar();
			self.prestamoService.cargar();

		}, function(error){
			alert(error);
		});

	},


	crear: function(formulario){

		var self = this;

		if(formulario.$valid){

			var data = angular.copy(self.pelicula.nueva);

			self.$http.post(self.accessPoint + '/peliculas/add',data).then(function(result){

				self.cargar();

				self.pelicula.mensaje = "Película creada. Se le redireccionará a la página de las películas.";

				self.utilService.delayRedireccion("/peliculas");

			}, function(error){

				alert(error);

			});

		}

	},

	lanzaEditar: function(pelicula_id){

		var self = this;

		self.pelicula.editar = self.utilService.buscarPelicula(pelicula_id);

		self.$location.path( "/peliculas/edit" );

	},

	editar: function(formulario){

		var self = this;

		if(formulario.$valid){

			var data = angular.copy(self.pelicula.editar);

			delete data.copia;

			self.$http.put(self.accessPoint + '/peliculas/update',data).then(function(result){

				self.cargar();

				self.pelicula.mensaje = "Película editada. Se le redireccionará a la página de las películas.";

				self.delayRedireccion();

			}, function(error){

				alert(error);

			});

		}

	},

	delayRedireccion: function(){

		var self = this;

		self.$timeout(function() {

			self.resetearEdicionPelicula();

			self.$location.path("/peliculas");

		}, 4000);


	}

	
});