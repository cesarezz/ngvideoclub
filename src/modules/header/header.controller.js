/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function headerController(headers,headerService,principalService){

	var self = this;

	self.headers = headers;
	self.headerService = headerService;
	self.principalService = principalService;

}